﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using dba_app;
namespace DatabaseTestConcurency
{
    [TestClass]
    public class concurencyTest
    {
        [TestMethod]
        public void DeadLockTestMethod1()
        {
            Console.WriteLine("Started: at " + DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff"));
            Parallel.For(0, 2, i =>
            {
                Task Task = new Task(TaskFunction);
                Task.Start();
                Console.WriteLine("Task " + i + " initiated at: " + DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff"));
                if (!Task.Wait(5000))
                {
                    Assert.Fail("Deadlock detected");
                }
            });
            Console.WriteLine("All done: at " + DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff"));
        }

        private void TaskFunction()
        {
            // do something that causes a deadlock here
            db dba = new db();
            if (dba.login("admin", "admin"))
            {
                Console.WriteLine("finished Task at: " + DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff"));
            }
            else
            {
                Assert.Fail("Login failed??");
            }
        }

        [TestMethod]
        public void DeadLockTestMethod2()
        {
            Console.WriteLine("Started: at " + DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff"));
            Parallel.For(0, 2, i =>
            {
                Task Task = new Task(TaskFunctionTwo);
                Task.Start();
                Console.WriteLine("Task " + i + " initiated at: " + DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff"));
                if (!Task.Wait(5000))
                {
                    Assert.Fail("Deadlock detected");
                }
            });
            Console.WriteLine("All done: at " + DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff"));
        }

        private void TaskFunctionTwo()
        {
            // do something that causes a deadlock here
            db dba = new db();
            if (dba.get_app_list()!=null)
            {
                Console.WriteLine("finished Task at: " + DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff"));
            }
            else
            {
                Assert.Fail("Null Table");
            }
        }
    }
}
