﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;

namespace dba_app
{
    public partial class app_store : MetroForm
    {
        private db dba = new db();

        private int u_id;
        private string username = null;

        public app_store(int user_id, bool is_dev, string username)
        {
            InitializeComponent();

            //get / set
            u_id = user_id;
            username = this.username;
            if (is_dev)         //-> if user is developer show appropriate links
                metroLink_dev.Visible = true;
            else
                metroLink_dev.Visible = false;
                        //welcome user
                metroLabel_user_name.Text = "Welcome " + username + "!";
            //fill grids on load    
            init_create_ui();
        }

        public void init_create_ui()
        {       //-> load winform grids on load
            load_table();
        }

        public void load_table()
        {       //-> load app list table from database
           metroGrid1_apps.DataSource =  dba.get_app_list();
        }

        private void launch_app_page(object sender, DataGridViewCellEventArgs e)
        {//-> handle purchase app button logic
            try
            { 
                DataGridViewButtonCell cell = (DataGridViewButtonCell)
                 metroGrid1_apps.Rows[e.RowIndex].Cells[e.ColumnIndex];     //-> Get clicked cell/ row

                string index = (string)metroGrid1_apps.Rows[e.RowIndex].Cells[2].Value.ToString();      //->Get relevant information from grids
                string title = (string)metroGrid1_apps.Rows[e.RowIndex].Cells[3].Value.ToString();
                string price = (string)metroGrid1_apps.Rows[e.RowIndex].Cells[4].Value.ToString(); 
                DialogResult trans = MetroFramework.MetroMessageBox.Show(this, "Would you like to add " + title + " to your devices for $" + price + "?",
                    "Add app to device", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);       //-> Launch confirmation dialog box

                if (trans == DialogResult.Yes)
                {       //-->user confirms download, create transaction record
                    int s_id;
                    Int32.TryParse(index, out s_id);
                    process_transaction(s_id);      //-> Process transaction function
                    MetroFramework.MetroMessageBox.Show(this, "The app has been added to your account, please view your account page for billing details!",
                        "Transaction Success", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);       //-> Error popup dialog
                }
            }catch
            {       //-> error handler
            }
        }

        public void process_transaction(int s_id)
        {       //-> Add a transaction report of downloaded app
            dba.process_transaction(s_id, u_id, "paypal");
        }

        private void open_account_page(object sender, EventArgs e)
        {       //-> Open account settings form
            account acc = new account(u_id);
            acc.Visible = true;
        }

        private void open_about_page(object sender, EventArgs e)
        {
            try
            {
                about ab = new about();
                ab.Visible = true;
            }
            catch
            {
                MetroFramework.MetroMessageBox.Show(this, "Flash Player Not Installed, or working!",
                         "Transaction Success", MessageBoxButtons.OK, MessageBoxIcon.Error);  
            }
        }

        private void open_dev_page(object sender, EventArgs e)
        {
            dev_portal devp = new dev_portal(u_id);
            devp.Visible = true;
        }

        private void refresh(object sender, EventArgs e)
        {
            load_table();
        }

    }
}
