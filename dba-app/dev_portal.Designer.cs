﻿namespace dba_app
{
    partial class dev_portal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.metroTabControl1 = new MetroFramework.Controls.MetroTabControl();
            this.metroTabPage1 = new MetroFramework.Controls.MetroTabPage();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroButton_upload = new MetroFramework.Controls.MetroButton();
            this.metroLabel_bool = new MetroFramework.Controls.MetroLabel();
            this.metroToggle = new MetroFramework.Controls.MetroToggle();
            this.metroLabel_cost = new MetroFramework.Controls.MetroLabel();
            this.metroTrackBar_cost = new MetroFramework.Controls.MetroTrackBar();
            this.metroTextBox_description = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel_title = new MetroFramework.Controls.MetroLabel();
            this.metroLabel_description = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox_title = new MetroFramework.Controls.MetroTextBox();
            this.metroTabPage2 = new MetroFramework.Controls.MetroTabPage();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.metroLabel_upload_new_app_title = new MetroFramework.Controls.MetroLabel();
            this.metroTabPage3 = new MetroFramework.Controls.MetroTabPage();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.metroTextBox_stats = new System.Windows.Forms.RichTextBox();
            this.metroTabControl1.SuspendLayout();
            this.metroTabPage1.SuspendLayout();
            this.metroTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.metroTabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // metroPanel1
            // 
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(24, 64);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(741, 509);
            this.metroPanel1.TabIndex = 0;
            this.metroPanel1.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // metroTabControl1
            // 
            this.metroTabControl1.Controls.Add(this.metroTabPage1);
            this.metroTabControl1.Controls.Add(this.metroTabPage2);
            this.metroTabControl1.Controls.Add(this.metroTabPage3);
            this.metroTabControl1.Location = new System.Drawing.Point(27, 63);
            this.metroTabControl1.Name = "metroTabControl1";
            this.metroTabControl1.SelectedIndex = 2;
            this.metroTabControl1.Size = new System.Drawing.Size(800, 527);
            this.metroTabControl1.Style = MetroFramework.MetroColorStyle.Orange;
            this.metroTabControl1.TabIndex = 2;
            this.metroTabControl1.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroTabControl1.UseSelectable = true;
            // 
            // metroTabPage1
            // 
            this.metroTabPage1.Controls.Add(this.metroLabel1);
            this.metroTabPage1.Controls.Add(this.metroButton_upload);
            this.metroTabPage1.Controls.Add(this.metroLabel_bool);
            this.metroTabPage1.Controls.Add(this.metroToggle);
            this.metroTabPage1.Controls.Add(this.metroLabel_cost);
            this.metroTabPage1.Controls.Add(this.metroTrackBar_cost);
            this.metroTabPage1.Controls.Add(this.metroTextBox_description);
            this.metroTabPage1.Controls.Add(this.metroLabel_title);
            this.metroTabPage1.Controls.Add(this.metroLabel_description);
            this.metroTabPage1.Controls.Add(this.metroTextBox_title);
            this.metroTabPage1.HorizontalScrollbarBarColor = true;
            this.metroTabPage1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage1.HorizontalScrollbarSize = 10;
            this.metroTabPage1.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage1.Name = "metroTabPage1";
            this.metroTabPage1.Size = new System.Drawing.Size(792, 485);
            this.metroTabPage1.Style = MetroFramework.MetroColorStyle.Orange;
            this.metroTabPage1.TabIndex = 3;
            this.metroTabPage1.Text = "Add App";
            this.metroTabPage1.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroTabPage1.VerticalScrollbarBarColor = true;
            this.metroTabPage1.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage1.VerticalScrollbarSize = 10;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel1.Location = new System.Drawing.Point(15, 17);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(142, 25);
            this.metroLabel1.Style = MetroFramework.MetroColorStyle.Lime;
            this.metroLabel1.TabIndex = 13;
            this.metroLabel1.Text = "Upload New App";
            this.metroLabel1.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroButton_upload
            // 
            this.metroButton_upload.Location = new System.Drawing.Point(12, 353);
            this.metroButton_upload.Name = "metroButton_upload";
            this.metroButton_upload.Size = new System.Drawing.Size(97, 23);
            this.metroButton_upload.Style = MetroFramework.MetroColorStyle.Orange;
            this.metroButton_upload.TabIndex = 12;
            this.metroButton_upload.Text = "Submit to Store";
            this.metroButton_upload.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroButton_upload.UseSelectable = true;
            this.metroButton_upload.Click += new System.EventHandler(this.add_app);
            // 
            // metroLabel_bool
            // 
            this.metroLabel_bool.AutoSize = true;
            this.metroLabel_bool.Location = new System.Drawing.Point(12, 295);
            this.metroLabel_bool.Name = "metroLabel_bool";
            this.metroLabel_bool.Size = new System.Drawing.Size(115, 19);
            this.metroLabel_bool.Style = MetroFramework.MetroColorStyle.Orange;
            this.metroLabel_bool.TabIndex = 11;
            this.metroLabel_bool.Text = "In App Purchases?";
            this.metroLabel_bool.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroToggle
            // 
            this.metroToggle.AutoSize = true;
            this.metroToggle.Location = new System.Drawing.Point(12, 317);
            this.metroToggle.Name = "metroToggle";
            this.metroToggle.Size = new System.Drawing.Size(80, 17);
            this.metroToggle.Style = MetroFramework.MetroColorStyle.Orange;
            this.metroToggle.TabIndex = 10;
            this.metroToggle.Text = "Off";
            this.metroToggle.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroToggle.UseSelectable = true;
            // 
            // metroLabel_cost
            // 
            this.metroLabel_cost.AutoSize = true;
            this.metroLabel_cost.Location = new System.Drawing.Point(15, 247);
            this.metroLabel_cost.Name = "metroLabel_cost";
            this.metroLabel_cost.Size = new System.Drawing.Size(35, 19);
            this.metroLabel_cost.Style = MetroFramework.MetroColorStyle.Orange;
            this.metroLabel_cost.TabIndex = 9;
            this.metroLabel_cost.Text = "Cost";
            this.metroLabel_cost.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroTrackBar_cost
            // 
            this.metroTrackBar_cost.BackColor = System.Drawing.Color.Transparent;
            this.metroTrackBar_cost.Location = new System.Drawing.Point(15, 269);
            this.metroTrackBar_cost.Maximum = 10;
            this.metroTrackBar_cost.MouseWheelBarPartitions = 3;
            this.metroTrackBar_cost.Name = "metroTrackBar_cost";
            this.metroTrackBar_cost.Size = new System.Drawing.Size(246, 23);
            this.metroTrackBar_cost.Style = MetroFramework.MetroColorStyle.Orange;
            this.metroTrackBar_cost.TabIndex = 8;
            this.metroTrackBar_cost.Text = "metroTrackBar1";
            this.metroTrackBar_cost.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroTrackBar_cost.Value = 0;
            this.metroTrackBar_cost.ValueChanged += new System.EventHandler(this.metroTrackBar_cost_SizeChanged);
            // 
            // metroTextBox_description
            // 
            this.metroTextBox_description.Lines = new string[0];
            this.metroTextBox_description.Location = new System.Drawing.Point(15, 132);
            this.metroTextBox_description.MaxLength = 32767;
            this.metroTextBox_description.Multiline = true;
            this.metroTextBox_description.Name = "metroTextBox_description";
            this.metroTextBox_description.PasswordChar = '\0';
            this.metroTextBox_description.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox_description.SelectedText = "";
            this.metroTextBox_description.Size = new System.Drawing.Size(246, 108);
            this.metroTextBox_description.Style = MetroFramework.MetroColorStyle.Orange;
            this.metroTextBox_description.TabIndex = 7;
            this.metroTextBox_description.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroTextBox_description.UseSelectable = true;
            // 
            // metroLabel_title
            // 
            this.metroLabel_title.AutoSize = true;
            this.metroLabel_title.Location = new System.Drawing.Point(15, 54);
            this.metroLabel_title.Name = "metroLabel_title";
            this.metroLabel_title.Size = new System.Drawing.Size(33, 19);
            this.metroLabel_title.Style = MetroFramework.MetroColorStyle.Orange;
            this.metroLabel_title.TabIndex = 6;
            this.metroLabel_title.Text = "Title";
            this.metroLabel_title.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroLabel_description
            // 
            this.metroLabel_description.AutoSize = true;
            this.metroLabel_description.Location = new System.Drawing.Point(15, 109);
            this.metroLabel_description.Name = "metroLabel_description";
            this.metroLabel_description.Size = new System.Drawing.Size(74, 19);
            this.metroLabel_description.Style = MetroFramework.MetroColorStyle.Orange;
            this.metroLabel_description.TabIndex = 5;
            this.metroLabel_description.Text = "Description";
            this.metroLabel_description.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroTextBox_title
            // 
            this.metroTextBox_title.Lines = new string[0];
            this.metroTextBox_title.Location = new System.Drawing.Point(15, 79);
            this.metroTextBox_title.MaxLength = 32767;
            this.metroTextBox_title.Name = "metroTextBox_title";
            this.metroTextBox_title.PasswordChar = '\0';
            this.metroTextBox_title.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox_title.SelectedText = "";
            this.metroTextBox_title.Size = new System.Drawing.Size(246, 23);
            this.metroTextBox_title.Style = MetroFramework.MetroColorStyle.Orange;
            this.metroTextBox_title.TabIndex = 4;
            this.metroTextBox_title.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroTextBox_title.UseSelectable = true;
            // 
            // metroTabPage2
            // 
            this.metroTabPage2.Controls.Add(this.dataGridView1);
            this.metroTabPage2.Controls.Add(this.metroLabel_upload_new_app_title);
            this.metroTabPage2.HorizontalScrollbarBarColor = true;
            this.metroTabPage2.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage2.HorizontalScrollbarSize = 10;
            this.metroTabPage2.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage2.Name = "metroTabPage2";
            this.metroTabPage2.Size = new System.Drawing.Size(792, 485);
            this.metroTabPage2.TabIndex = 4;
            this.metroTabPage2.Text = "Edit Apps";
            this.metroTabPage2.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroTabPage2.VerticalScrollbarBarColor = true;
            this.metroTabPage2.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage2.VerticalScrollbarSize = 10;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(24, 63);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(747, 419);
            this.dataGridView1.TabIndex = 15;
            this.dataGridView1.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.edit_cell);
            this.dataGridView1.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.delete_row);
            // 
            // metroLabel_upload_new_app_title
            // 
            this.metroLabel_upload_new_app_title.AutoSize = true;
            this.metroLabel_upload_new_app_title.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel_upload_new_app_title.Location = new System.Drawing.Point(24, 23);
            this.metroLabel_upload_new_app_title.Name = "metroLabel_upload_new_app_title";
            this.metroLabel_upload_new_app_title.Size = new System.Drawing.Size(111, 25);
            this.metroLabel_upload_new_app_title.Style = MetroFramework.MetroColorStyle.Orange;
            this.metroLabel_upload_new_app_title.TabIndex = 14;
            this.metroLabel_upload_new_app_title.Text = "Edit My Apps";
            this.metroLabel_upload_new_app_title.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroTabPage3
            // 
            this.metroTabPage3.Controls.Add(this.metroTextBox_stats);
            this.metroTabPage3.HorizontalScrollbarBarColor = true;
            this.metroTabPage3.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage3.HorizontalScrollbarSize = 10;
            this.metroTabPage3.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage3.Name = "metroTabPage3";
            this.metroTabPage3.Size = new System.Drawing.Size(792, 485);
            this.metroTabPage3.TabIndex = 5;
            this.metroTabPage3.Text = "My Statistics";
            this.metroTabPage3.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroTabPage3.VerticalScrollbarBarColor = true;
            this.metroTabPage3.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage3.VerticalScrollbarSize = 10;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // metroTextBox_stats
            // 
            this.metroTextBox_stats.BackColor = System.Drawing.Color.Black;
            this.metroTextBox_stats.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.metroTextBox_stats.Location = new System.Drawing.Point(0, 0);
            this.metroTextBox_stats.Name = "metroTextBox_stats";
            this.metroTextBox_stats.ReadOnly = true;
            this.metroTextBox_stats.Size = new System.Drawing.Size(434, 361);
            this.metroTextBox_stats.TabIndex = 3;
            this.metroTextBox_stats.Text = "";
            // 
            // dev_portal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(850, 625);
            this.Controls.Add(this.metroTabControl1);
            this.Controls.Add(this.metroPanel1);
            this.Name = "dev_portal";
            this.Style = MetroFramework.MetroColorStyle.Orange;
            this.Text = "Developer Portal";
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroTabControl1.ResumeLayout(false);
            this.metroTabPage1.ResumeLayout(false);
            this.metroTabPage1.PerformLayout();
            this.metroTabPage2.ResumeLayout(false);
            this.metroTabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.metroTabPage3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Controls.MetroTabControl metroTabControl1;
        private MetroFramework.Controls.MetroTabPage metroTabPage1;
        private MetroFramework.Controls.MetroTabPage metroTabPage2;
        private MetroFramework.Controls.MetroTabPage metroTabPage3;
        private MetroFramework.Controls.MetroLabel metroLabel_cost;
        private MetroFramework.Controls.MetroTrackBar metroTrackBar_cost;
        private MetroFramework.Controls.MetroTextBox metroTextBox_description;
        private MetroFramework.Controls.MetroLabel metroLabel_title;
        private MetroFramework.Controls.MetroLabel metroLabel_description;
        private MetroFramework.Controls.MetroTextBox metroTextBox_title;
        private MetroFramework.Controls.MetroButton metroButton_upload;
        private MetroFramework.Controls.MetroLabel metroLabel_bool;
        private MetroFramework.Controls.MetroToggle metroToggle;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel_upload_new_app_title;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.RichTextBox metroTextBox_stats;
    }
}