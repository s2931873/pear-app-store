﻿namespace dba_app
{
    partial class form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(form1));
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroButton1_submit = new MetroFramework.Controls.MetroButton();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox1_uname = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBox2_pword = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4_login_output = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox1_create_password = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox2_create_email = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroButton1 = new MetroFramework.Controls.MetroButton();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox3_create_phone = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox4_create_name = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.metroDateTime1 = new MetroFramework.Controls.MetroDateTime();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel11 = new MetroFramework.Controls.MetroLabel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.metroToggle = new MetroFramework.Controls.MetroToggle();
            this.metroCheckBoxGender = new MetroFramework.Controls.MetroCheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel1.Location = new System.Drawing.Point(35, 103);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(121, 25);
            this.metroLabel1.Style = MetroFramework.MetroColorStyle.Lime;
            this.metroLabel1.TabIndex = 0;
            this.metroLabel1.Text = "Account Login";
            this.metroLabel1.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroButton1_submit
            // 
            this.metroButton1_submit.Location = new System.Drawing.Point(35, 266);
            this.metroButton1_submit.Name = "metroButton1_submit";
            this.metroButton1_submit.Size = new System.Drawing.Size(75, 23);
            this.metroButton1_submit.Style = MetroFramework.MetroColorStyle.Lime;
            this.metroButton1_submit.TabIndex = 1;
            this.metroButton1_submit.Text = "Submit";
            this.metroButton1_submit.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroButton1_submit.UseSelectable = true;
            this.metroButton1_submit.Click += new System.EventHandler(this.login_attempt_btn);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(35, 144);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(41, 19);
            this.metroLabel2.Style = MetroFramework.MetroColorStyle.Lime;
            this.metroLabel2.TabIndex = 2;
            this.metroLabel2.Text = "Email";
            this.metroLabel2.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroTextBox1_uname
            // 
            this.metroTextBox1_uname.Lines = new string[0];
            this.metroTextBox1_uname.Location = new System.Drawing.Point(35, 166);
            this.metroTextBox1_uname.MaxLength = 32767;
            this.metroTextBox1_uname.Name = "metroTextBox1_uname";
            this.metroTextBox1_uname.PasswordChar = '\0';
            this.metroTextBox1_uname.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox1_uname.SelectedText = "";
            this.metroTextBox1_uname.Size = new System.Drawing.Size(294, 23);
            this.metroTextBox1_uname.Style = MetroFramework.MetroColorStyle.Lime;
            this.metroTextBox1_uname.TabIndex = 3;
            this.metroTextBox1_uname.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroTextBox1_uname.UseSelectable = true;
            this.metroTextBox1_uname.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.password_key_bindings_submit);
            // 
            // metroTextBox2_pword
            // 
            this.metroTextBox2_pword.Lines = new string[0];
            this.metroTextBox2_pword.Location = new System.Drawing.Point(35, 224);
            this.metroTextBox2_pword.MaxLength = 32767;
            this.metroTextBox2_pword.Name = "metroTextBox2_pword";
            this.metroTextBox2_pword.PasswordChar = '●';
            this.metroTextBox2_pword.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox2_pword.SelectedText = "";
            this.metroTextBox2_pword.Size = new System.Drawing.Size(294, 23);
            this.metroTextBox2_pword.Style = MetroFramework.MetroColorStyle.Lime;
            this.metroTextBox2_pword.TabIndex = 5;
            this.metroTextBox2_pword.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroTextBox2_pword.UseSelectable = true;
            this.metroTextBox2_pword.UseSystemPasswordChar = true;
            this.metroTextBox2_pword.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.password_key_bindings_submit);
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(35, 202);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(63, 19);
            this.metroLabel3.Style = MetroFramework.MetroColorStyle.Lime;
            this.metroLabel3.TabIndex = 4;
            this.metroLabel3.Text = "Password";
            this.metroLabel3.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroLabel4_login_output
            // 
            this.metroLabel4_login_output.AutoSize = true;
            this.metroLabel4_login_output.Location = new System.Drawing.Point(35, 309);
            this.metroLabel4_login_output.Name = "metroLabel4_login_output";
            this.metroLabel4_login_output.Size = new System.Drawing.Size(0, 0);
            this.metroLabel4_login_output.Style = MetroFramework.MetroColorStyle.Lime;
            this.metroLabel4_login_output.TabIndex = 6;
            this.metroLabel4_login_output.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(391, 309);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(0, 0);
            this.metroLabel4.Style = MetroFramework.MetroColorStyle.Lime;
            this.metroLabel4.TabIndex = 13;
            this.metroLabel4.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroTextBox1_create_password
            // 
            this.metroTextBox1_create_password.Lines = new string[0];
            this.metroTextBox1_create_password.Location = new System.Drawing.Point(391, 224);
            this.metroTextBox1_create_password.MaxLength = 32767;
            this.metroTextBox1_create_password.Name = "metroTextBox1_create_password";
            this.metroTextBox1_create_password.PasswordChar = '●';
            this.metroTextBox1_create_password.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox1_create_password.SelectedText = "";
            this.metroTextBox1_create_password.Size = new System.Drawing.Size(294, 23);
            this.metroTextBox1_create_password.Style = MetroFramework.MetroColorStyle.Lime;
            this.metroTextBox1_create_password.TabIndex = 12;
            this.metroTextBox1_create_password.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroTextBox1_create_password.UseSelectable = true;
            this.metroTextBox1_create_password.UseSystemPasswordChar = true;
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(391, 202);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(63, 19);
            this.metroLabel5.Style = MetroFramework.MetroColorStyle.Lime;
            this.metroLabel5.TabIndex = 11;
            this.metroLabel5.Text = "Password";
            this.metroLabel5.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroTextBox2_create_email
            // 
            this.metroTextBox2_create_email.Lines = new string[0];
            this.metroTextBox2_create_email.Location = new System.Drawing.Point(391, 166);
            this.metroTextBox2_create_email.MaxLength = 32767;
            this.metroTextBox2_create_email.Name = "metroTextBox2_create_email";
            this.metroTextBox2_create_email.PasswordChar = '\0';
            this.metroTextBox2_create_email.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox2_create_email.SelectedText = "";
            this.metroTextBox2_create_email.Size = new System.Drawing.Size(294, 23);
            this.metroTextBox2_create_email.Style = MetroFramework.MetroColorStyle.Lime;
            this.metroTextBox2_create_email.TabIndex = 10;
            this.metroTextBox2_create_email.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroTextBox2_create_email.UseSelectable = true;
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(391, 144);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(41, 19);
            this.metroLabel6.Style = MetroFramework.MetroColorStyle.Lime;
            this.metroLabel6.TabIndex = 9;
            this.metroLabel6.Text = "Email";
            this.metroLabel6.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroButton1
            // 
            this.metroButton1.Location = new System.Drawing.Point(391, 572);
            this.metroButton1.Name = "metroButton1";
            this.metroButton1.Size = new System.Drawing.Size(75, 23);
            this.metroButton1.Style = MetroFramework.MetroColorStyle.Lime;
            this.metroButton1.TabIndex = 8;
            this.metroButton1.Text = "Submit";
            this.metroButton1.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroButton1.UseSelectable = true;
            this.metroButton1.Click += new System.EventHandler(this.create_account);
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel7.Location = new System.Drawing.Point(391, 103);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(128, 25);
            this.metroLabel7.Style = MetroFramework.MetroColorStyle.Lime;
            this.metroLabel7.TabIndex = 7;
            this.metroLabel7.Text = "Create Account";
            this.metroLabel7.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroTextBox3_create_phone
            // 
            this.metroTextBox3_create_phone.Lines = new string[0];
            this.metroTextBox3_create_phone.Location = new System.Drawing.Point(391, 385);
            this.metroTextBox3_create_phone.MaxLength = 32767;
            this.metroTextBox3_create_phone.Name = "metroTextBox3_create_phone";
            this.metroTextBox3_create_phone.PasswordChar = '\0';
            this.metroTextBox3_create_phone.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox3_create_phone.SelectedText = "";
            this.metroTextBox3_create_phone.Size = new System.Drawing.Size(294, 23);
            this.metroTextBox3_create_phone.Style = MetroFramework.MetroColorStyle.Lime;
            this.metroTextBox3_create_phone.TabIndex = 15;
            this.metroTextBox3_create_phone.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroTextBox3_create_phone.UseSelectable = true;
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.Location = new System.Drawing.Point(391, 363);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(46, 19);
            this.metroLabel8.Style = MetroFramework.MetroColorStyle.Lime;
            this.metroLabel8.TabIndex = 14;
            this.metroLabel8.Text = "Phone";
            this.metroLabel8.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroTextBox4_create_name
            // 
            this.metroTextBox4_create_name.Lines = new string[0];
            this.metroTextBox4_create_name.Location = new System.Drawing.Point(391, 286);
            this.metroTextBox4_create_name.MaxLength = 32767;
            this.metroTextBox4_create_name.Name = "metroTextBox4_create_name";
            this.metroTextBox4_create_name.PasswordChar = '\0';
            this.metroTextBox4_create_name.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox4_create_name.SelectedText = "";
            this.metroTextBox4_create_name.Size = new System.Drawing.Size(294, 23);
            this.metroTextBox4_create_name.Style = MetroFramework.MetroColorStyle.Lime;
            this.metroTextBox4_create_name.TabIndex = 17;
            this.metroTextBox4_create_name.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroTextBox4_create_name.UseSelectable = true;
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.Location = new System.Drawing.Point(391, 264);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(69, 19);
            this.metroLabel9.Style = MetroFramework.MetroColorStyle.Lime;
            this.metroLabel9.TabIndex = 16;
            this.metroLabel9.Text = "Full Name";
            this.metroLabel9.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroDateTime1
            // 
            this.metroDateTime1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.metroDateTime1.Location = new System.Drawing.Point(391, 446);
            this.metroDateTime1.MinimumSize = new System.Drawing.Size(0, 29);
            this.metroDateTime1.Name = "metroDateTime1";
            this.metroDateTime1.Size = new System.Drawing.Size(200, 29);
            this.metroDateTime1.Style = MetroFramework.MetroColorStyle.Lime;
            this.metroDateTime1.TabIndex = 18;
            this.metroDateTime1.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroLabel10
            // 
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.Location = new System.Drawing.Point(391, 424);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(84, 19);
            this.metroLabel10.Style = MetroFramework.MetroColorStyle.Lime;
            this.metroLabel10.TabIndex = 19;
            this.metroLabel10.Text = "Date of Birth";
            this.metroLabel10.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroLabel11
            // 
            this.metroLabel11.AutoSize = true;
            this.metroLabel11.Location = new System.Drawing.Point(391, 498);
            this.metroLabel11.Name = "metroLabel11";
            this.metroLabel11.Size = new System.Drawing.Size(120, 19);
            this.metroLabel11.Style = MetroFramework.MetroColorStyle.Lime;
            this.metroLabel11.TabIndex = 22;
            this.metroLabel11.Text = "Developer Account";
            this.metroLabel11.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(191, 11);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(54, 71);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 23;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Linux Libertine G", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.SeaGreen;
            this.label1.Location = new System.Drawing.Point(23, 60);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(157, 22);
            this.label1.TabIndex = 24;
            this.label1.Text = "Powered by HTML5";
            // 
            // metroToggle
            // 
            this.metroToggle.AutoSize = true;
            this.metroToggle.Location = new System.Drawing.Point(391, 532);
            this.metroToggle.Name = "metroToggle";
            this.metroToggle.Size = new System.Drawing.Size(80, 17);
            this.metroToggle.TabIndex = 25;
            this.metroToggle.Text = "Off";
            this.metroToggle.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroToggle.UseSelectable = true;
            // 
            // metroCheckBoxGender
            // 
            this.metroCheckBoxGender.AutoSize = true;
            this.metroCheckBoxGender.Checked = true;
            this.metroCheckBoxGender.CheckState = System.Windows.Forms.CheckState.Checked;
            this.metroCheckBoxGender.Location = new System.Drawing.Point(391, 328);
            this.metroCheckBoxGender.Name = "metroCheckBoxGender";
            this.metroCheckBoxGender.Size = new System.Drawing.Size(93, 15);
            this.metroCheckBoxGender.Style = MetroFramework.MetroColorStyle.Lime;
            this.metroCheckBoxGender.TabIndex = 26;
            this.metroCheckBoxGender.Text = "Gender: Male";
            this.metroCheckBoxGender.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroCheckBoxGender.UseSelectable = true;
            this.metroCheckBoxGender.CheckedChanged += new System.EventHandler(this.change_gender_textbox_text);
            // 
            // form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(952, 767);
            this.Controls.Add(this.metroCheckBoxGender);
            this.Controls.Add(this.metroToggle);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.metroLabel11);
            this.Controls.Add(this.metroLabel10);
            this.Controls.Add(this.metroDateTime1);
            this.Controls.Add(this.metroTextBox4_create_name);
            this.Controls.Add(this.metroLabel9);
            this.Controls.Add(this.metroTextBox3_create_phone);
            this.Controls.Add(this.metroLabel8);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.metroTextBox1_create_password);
            this.Controls.Add(this.metroLabel5);
            this.Controls.Add(this.metroTextBox2_create_email);
            this.Controls.Add(this.metroLabel6);
            this.Controls.Add(this.metroButton1);
            this.Controls.Add(this.metroLabel7);
            this.Controls.Add(this.metroLabel4_login_output);
            this.Controls.Add(this.metroTextBox2_pword);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.metroTextBox1_uname);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.metroButton1_submit);
            this.Controls.Add(this.metroLabel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "form1";
            this.Style = MetroFramework.MetroColorStyle.Lime;
            this.Text = "Pear App Store";
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroButton metroButton1_submit;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroTextBox metroTextBox1_uname;
        private MetroFramework.Controls.MetroTextBox metroTextBox2_pword;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroLabel metroLabel4_login_output;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroTextBox metroTextBox1_create_password;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroTextBox metroTextBox2_create_email;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroButton metroButton1;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroTextBox metroTextBox3_create_phone;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroTextBox metroTextBox4_create_name;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroDateTime metroDateTime1;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private MetroFramework.Controls.MetroLabel metroLabel11;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private MetroFramework.Controls.MetroToggle metroToggle;
        private MetroFramework.Controls.MetroCheckBox metroCheckBoxGender;


    }
}

