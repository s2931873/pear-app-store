﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;

namespace dba_app
{
    public partial class dev_portal : MetroForm
    {
        private db_dev dba = new db_dev();
        private int u_id;
        public dev_portal(int index)
        {
            InitializeComponent();
            u_id = index;

            load_table();
        }

        public void load_table()
        {       //-> load app list table from database
            dataGridView1.DataSource = dba.get_developers_apps_to_grid(u_id);
            metroTextBox_stats.Text = dba.get_developer_statistics_downloads(u_id);
        }

        private void add_app(object sender, EventArgs e)
        {
            string title = metroTextBox_title.Text.ToString();
            string description = metroTextBox_description.Text.ToString();
            double cost = metroTrackBar_cost.Value;
            bool inapp = metroToggle.Checked;
            string package_p;
            
            var package = new OpenFileDialog();
            package.Title = "Upload your software!";
            

            if (package.ShowDialog() == DialogResult.OK) 
            {
                package_p = package.FileName;
                dba.add_app_to_store(title, description, cost, u_id, inapp, package_p);
                DialogResult trans = MetroFramework.MetroMessageBox.Show(this, "Software Added!",
                   "Add app to device", MessageBoxButtons.OK, MessageBoxIcon.Information);       
                load_table();
            }
        }

        private void metroTrackBar_cost_SizeChanged(object sender, EventArgs e)
        {
            metroLabel_cost.Text = "Cost: $" + metroTrackBar_cost.Value;
        }

        private void delete_row(object sender, DataGridViewRowCancelEventArgs e)
        {
            int Index = (Int32)dataGridView1.CurrentRow.Cells[0].Value;
            DialogResult del = MetroFramework.MetroMessageBox.Show(this, "Delete App?",
                 "Add app to device", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if(del == DialogResult.Yes)
            {
                dba.delete_software(u_id, Index);
                load_table();
            }
        }

        private void edit_cell(object sender, DataGridViewCellEventArgs e)
        {
            ArrayList arr = new ArrayList(6);
            int Index = (Int32)dataGridView1.CurrentRow.Cells[0].Value;
            int cIndex = e.ColumnIndex;   
   
            var currentRowx = dataGridView1.CurrentCell.Value.ToString();
            var currentRowy = dataGridView1.Columns[cIndex].HeaderText.ToString();
            int cellIndex = dataGridView1.CurrentCell.ColumnIndex;

            dba.update_software_tuple(Index, u_id, currentRowy, currentRowx);
            DialogResult trans = MetroFramework.MetroMessageBox.Show(this, "Software Updated!",
                   "Add app to device", MessageBoxButtons.OK, MessageBoxIcon.Information);
            load_table();
        }
    }
}
