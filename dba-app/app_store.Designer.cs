﻿namespace dba_app
{
    partial class app_store
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(app_store));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.metroGrid1_apps = new MetroFramework.Controls.MetroGrid();
            this.label1 = new System.Windows.Forms.Label();
            this.metroLabel_user_name = new MetroFramework.Controls.MetroLabel();
            this.metroLink_account = new MetroFramework.Controls.MetroLink();
            this.metroLink_dev = new MetroFramework.Controls.MetroLink();
            this.metroLink_about = new MetroFramework.Controls.MetroLink();
            this.dataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.metroButton1 = new MetroFramework.Controls.MetroButton();
            this.icon = new System.Windows.Forms.DataGridViewImageColumn();
            this.Visit = new System.Windows.Forms.DataGridViewButtonColumn();
            ((System.ComponentModel.ISupportInitialize)(this.metroGrid1_apps)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // metroGrid1_apps
            // 
            this.metroGrid1_apps.AllowUserToResizeRows = false;
            this.metroGrid1_apps.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.metroGrid1_apps.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.metroGrid1_apps.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.metroGrid1_apps.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.metroGrid1_apps.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.metroGrid1_apps.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGrid1_apps.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.metroGrid1_apps.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.metroGrid1_apps.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.icon,
            this.Visit});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.metroGrid1_apps.DefaultCellStyle = dataGridViewCellStyle3;
            this.metroGrid1_apps.EnableHeadersVisualStyles = false;
            this.metroGrid1_apps.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.metroGrid1_apps.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            this.metroGrid1_apps.Location = new System.Drawing.Point(23, 149);
            this.metroGrid1_apps.Name = "metroGrid1_apps";
            this.metroGrid1_apps.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metroGrid1_apps.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.metroGrid1_apps.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.metroGrid1_apps.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.metroGrid1_apps.Size = new System.Drawing.Size(1280, 600);
            this.metroGrid1_apps.TabIndex = 0;
            this.metroGrid1_apps.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroGrid1_apps.UseCustomBackColor = true;
            this.metroGrid1_apps.UseCustomForeColor = true;
            this.metroGrid1_apps.UseStyleColors = true;
            this.metroGrid1_apps.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.launch_app_page);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Linux Libertine G", 14.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.SeaGreen;
            this.label1.Location = new System.Drawing.Point(23, 60);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(157, 22);
            this.label1.TabIndex = 26;
            this.label1.Text = "Powered by HTML5";
            // 
            // metroLabel_user_name
            // 
            this.metroLabel_user_name.AutoSize = true;
            this.metroLabel_user_name.Location = new System.Drawing.Point(24, 124);
            this.metroLabel_user_name.Name = "metroLabel_user_name";
            this.metroLabel_user_name.Size = new System.Drawing.Size(0, 0);
            this.metroLabel_user_name.TabIndex = 27;
            this.metroLabel_user_name.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // metroLink_account
            // 
            this.metroLink_account.FontSize = MetroFramework.MetroLinkSize.Medium;
            this.metroLink_account.Location = new System.Drawing.Point(421, 29);
            this.metroLink_account.Name = "metroLink_account";
            this.metroLink_account.Size = new System.Drawing.Size(149, 23);
            this.metroLink_account.TabIndex = 28;
            this.metroLink_account.Text = "My Account";
            this.metroLink_account.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLink_account.UseSelectable = true;
            this.metroLink_account.Click += new System.EventHandler(this.open_account_page);
            // 
            // metroLink_dev
            // 
            this.metroLink_dev.FontSize = MetroFramework.MetroLinkSize.Medium;
            this.metroLink_dev.Location = new System.Drawing.Point(576, 29);
            this.metroLink_dev.Name = "metroLink_dev";
            this.metroLink_dev.Size = new System.Drawing.Size(149, 23);
            this.metroLink_dev.TabIndex = 29;
            this.metroLink_dev.Text = "Developer Account";
            this.metroLink_dev.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLink_dev.UseSelectable = true;
            this.metroLink_dev.Click += new System.EventHandler(this.open_dev_page);
            // 
            // metroLink_about
            // 
            this.metroLink_about.FontSize = MetroFramework.MetroLinkSize.Medium;
            this.metroLink_about.Location = new System.Drawing.Point(292, 29);
            this.metroLink_about.Name = "metroLink_about";
            this.metroLink_about.Size = new System.Drawing.Size(149, 23);
            this.metroLink_about.TabIndex = 30;
            this.metroLink_about.Text = "About Us";
            this.metroLink_about.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metroLink_about.UseSelectable = true;
            this.metroLink_about.Click += new System.EventHandler(this.open_about_page);
            // 
            // dataGridViewImageColumn1
            // 
            this.dataGridViewImageColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dataGridViewImageColumn1.Frozen = true;
            this.dataGridViewImageColumn1.HeaderText = "";
            this.dataGridViewImageColumn1.Image = global::dba_app.Properties.Resources.pear;
            this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
            this.dataGridViewImageColumn1.ReadOnly = true;
            this.dataGridViewImageColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewImageColumn1.ToolTipText = "Apps Icon";
            this.dataGridViewImageColumn1.Width = 75;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(191, 11);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(54, 71);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 25;
            this.pictureBox1.TabStop = false;
            // 
            // metroButton1
            // 
            this.metroButton1.Location = new System.Drawing.Point(137, 124);
            this.metroButton1.Name = "metroButton1";
            this.metroButton1.Size = new System.Drawing.Size(43, 23);
            this.metroButton1.TabIndex = 31;
            this.metroButton1.Text = "refresh";
            this.metroButton1.UseSelectable = true;
            this.metroButton1.Click += new System.EventHandler(this.refresh);
            // 
            // icon
            // 
            this.icon.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.icon.Frozen = true;
            this.icon.HeaderText = "";
            this.icon.Image = ((System.Drawing.Image)(resources.GetObject("icon.Image")));
            this.icon.Name = "icon";
            this.icon.ReadOnly = true;
            this.icon.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.icon.ToolTipText = "Apps Icon";
            this.icon.Width = 75;
            // 
            // Visit
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.ButtonFace;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            this.Visit.DefaultCellStyle = dataGridViewCellStyle2;
            this.Visit.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.Visit.HeaderText = "Add to Cart";
            this.Visit.Name = "Visit";
            this.Visit.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Visit.Text = "Add to Cart";
            this.Visit.ToolTipText = "Open information page and Install";
            this.Visit.Width = 70;
            // 
            // app_store
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(810, 580);
            this.Controls.Add(this.metroButton1);
            this.Controls.Add(this.metroLink_about);
            this.Controls.Add(this.metroLink_dev);
            this.Controls.Add(this.metroLink_account);
            this.Controls.Add(this.metroLabel_user_name);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.metroGrid1_apps);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "app_store";
            this.ShadowType = MetroFramework.Forms.MetroFormShadowType.DropShadow;
            this.Text = "Pear App Store";
            this.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.metroGrid1_apps)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroGrid metroGrid1_apps;
        private System.Windows.Forms.Label label1;
        private MetroFramework.Controls.MetroLabel metroLabel_user_name;
        private MetroFramework.Controls.MetroLink metroLink_account;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn1;
        private MetroFramework.Controls.MetroLink metroLink_dev;
        private MetroFramework.Controls.MetroLink metroLink_about;
        private System.Windows.Forms.PictureBox pictureBox1;
        private MetroFramework.Controls.MetroButton metroButton1;
        private System.Windows.Forms.DataGridViewImageColumn icon;
        private System.Windows.Forms.DataGridViewButtonColumn Visit;
    }
}