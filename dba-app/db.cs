﻿using System;
using System.Collections;
using System.Text;
using MySql.Data.MySqlClient;
using System.Data;
using System.Security.Cryptography;
using System.Threading;

namespace dba_app
{
    class db
    {       //-> functions to access data from DBMS within forms
                //-> used in forms to maintain states
        public int user_id;
        public bool is_dev;

        protected MySqlConnection conn;

        public db(string user = "user")
        {
            string cs = @"server=localhost;userid="+ user +
            ";password=;database=appstore";

            try
            {
                conn = new MySqlConnection(cs);

                conn.Open();

                Console.WriteLine("MySQL version : {0}", conn.ServerVersion);
            }
            catch (Exception)
            {   
                throw;
            }
            finally
            {
                if (conn != null)
                    conn.Close();
            }
        }

        public bool login(string uname, string pword)
        {       //-> look up user through email and compare md5 ciphers
            int retryCount = 3;
            bool success = false;
            while (retryCount > 0 && !success)
            {
                MySqlDataReader rdr = null;
                try
                {
                    conn.Open();

                    string stm = "SELECT user_id, user_name, password, dev_id FROM view_login WHERE user_name = '" + uname + "' LIMIT 1";
                    MySqlCommand cmd = new MySqlCommand(stm, conn);
                    rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {       //-> compare input and database enteriess
                        if (uname == rdr.GetString(1) && md5_encrypt(pword) == rdr.GetString(2))
                        {       //-> additional meta-data
                            user_id = rdr.GetInt32(0);

                            is_dev = rdr.GetBoolean(3);
                            success = true;
                            return true;
                        }
                    }
                    success = true;
                    return false;
                }
                catch (MySqlException ex)
                {
                    if (ex.Number != 1205)
                    {
                        // a sql exception that is not a deadlock 
                        throw;
                    }
                    // Add delay here if you wish. 
                    Thread.Sleep(500);
                    retryCount--;
                    if (retryCount == 0) throw;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {       //-> clean up open connections
                    if (rdr != null)
                        rdr.Close();

                    if (conn != null)
                        conn.Close();
                }
            }
            return false;
        }
        public async void create_account(string email, string password, DateTime dob, char gender, string phone, string name, int dev)
        {       //-> insert user account into database
            int retryCount = 3;
            bool success = false;
            MySqlCommand cmd = new MySqlCommand();
            while (retryCount > 0 && !success)
            {
                try
                {
                    conn.Open();


                    cmd.Connection = conn;
                    cmd.CommandText = "START TRANSACTION; ";
                    cmd.CommandText += "INSERT INTO users(user_name, password, name, dob, gender, phone, dev_id)" +
                        "VALUES(@user_name, @password, @name, @dob, @gender, @phone, @dev_id);";

                    cmd.CommandText += "COMMIT; ";
                    cmd.Prepare();
                    //-> add parameters of this function to insert statement
                    cmd.Parameters.AddWithValue("@user_name", email);
                    cmd.Parameters.AddWithValue("@password", md5_encrypt(password));
                    cmd.Parameters.AddWithValue("@name", name);
                    cmd.Parameters.AddWithValue("@dob", dob);
                    cmd.Parameters.AddWithValue("@gender", gender);
                    cmd.Parameters.AddWithValue("@phone", phone);
                    cmd.Parameters.AddWithValue("@dev_id", dev);

                    await cmd.ExecuteNonQueryAsync();
                    success = true;
                }
                catch (MySqlException ex)
                {       //-> error handline
                    rollback(cmd);
                    if (ex.Number != 1205)
                    {
                        // a sql exception that is not a deadlock 
                        throw;
                    }
                    // Add delay here if you wish. 
                    Thread.Sleep(500);
                    retryCount--;
                    if (retryCount == 0) throw;
                }
                finally
                {
                    if (conn != null)
                        conn.Close();
                }
            }
        }

        public DataTable get_app_list()
        {       //->load all apps into datatable for datagridview
            DataTable dt = null;
            int retryCount = 3;
            bool success = false;
            while (retryCount > 0 && !success)
            {
                try
                {
                    conn.Open();
                    //-> select relevant entites for collumns
                    string stm = "SELECT * FROM view_apps";
                    MySqlDataAdapter da = new MySqlDataAdapter(stm, conn);

                    dt = new DataTable();

                    da.Fill(dt);

                    if (conn != null)
                        conn.Close();
                    success = true;
                    return dt;
                }catch(MySqlException ex)
                {
                    if (ex.Number != 1205)
                    {
                        // a sql exception that is not a deadlock 
                        throw;
                    }
                    // Add delay here if you wish. 
                    Thread.Sleep(500);
                    retryCount--;
                    if (retryCount == 0) throw;
                }
            }
            return dt;
        }

        public ArrayList get_user_account_details(int user_id)
        {       //->load current account details for updating 
            ArrayList arr = new ArrayList();
            int retryCount = 3;
            bool success = false;
            while (retryCount > 0 && !success)
            {
                try
                {
                    MySqlDataReader rdr = null;
                    conn.Open();

                    string stm = "SELECT user_name, name, phone FROM view_login WHERE user_id = " + user_id;

                    MySqlCommand cmd = new MySqlCommand(stm, conn);
                    rdr = cmd.ExecuteReader();

                    rdr.Read();

                    arr.Add(rdr.GetString(0));
                    arr.Add(rdr.GetString(1));
                    arr.Add(rdr.GetString(2));
                   

                    if (conn != null)
                        conn.Close();
                    if (rdr != null)
                        rdr.Close();

                    success = true;
                    return arr;
                }catch(MySqlException ex){
                    if (ex.Number != 1205)
                    {
                        // a sql exception that is not a deadlock 
                        throw;
                    }
                    // Add delay here if you wish. 
                    Thread.Sleep(500);
                    retryCount--;
                    if (retryCount == 0) throw;
                }
            }
            return arr;
        }

        public bool set_user_account_details(int uid, string email, string password, string name, DateTime dob, string phone)
        {       //-> update user account details
             int retryCount = 3;
            bool success = false;
            MySqlCommand cmd = new MySqlCommand();
            while (retryCount > 0 && !success)
            {
                Console.WriteLine(uid);
                try
                {
                        cmd.Connection = conn;
                        cmd.CommandText = "START TRANSACTION; ";
                        cmd.CommandText += "UPDATE users SET user_name = @user_name, password = @password, " +
                            "name = @name, dob = @dob, phone= @phone WHERE user_id = " + uid + " ; ";
                        cmd.CommandText += "COMMIT; ";
                        conn.Open();
                        cmd.Prepare();
                        //->add parameters to query
                        cmd.Parameters.AddWithValue("@user_name", email);
                        cmd.Parameters.AddWithValue("@password", md5_encrypt(password.ToString()));
                        cmd.Parameters.AddWithValue("@name", name);       //-> Paypal, direct deposit, etc.
                        cmd.Parameters.AddWithValue("@dob", dob.ToString());       //->Datetime now in current timezone
                        cmd.Parameters.AddWithValue("@phone", phone);       //-> we are not actually using a bank, so its always a valid payment
                        cmd.ExecuteNonQuery();
                        success = true;
                        if (conn != null)
                            conn.Close();

                        return true;
                }
                catch (MySqlException ex)
                {
                    rollback(cmd);

                    if (ex.Number != 1205)
                    {
                        // a sql exception that is not a deadlock 
                        throw;
                    }
                    // Add delay here if you wish.
                    Thread.Sleep(500);
                    retryCount--;
                    if (retryCount == 0) throw;
                }
            }
             return false;
            }

        public async void process_transaction(int sid, int uid, string p_method)
        {       //->record users transaction details upon app purchase
            int retryCount = 3;
            bool success = false;
            MySqlCommand cmd = new MySqlCommand();
            while (retryCount > 0 && !success)
            {
                try
                {
                    // your sql here
                    success = true;
                    conn.Open();

                    //->insert relevant data
                    cmd.Connection = conn;
                    cmd.CommandText = "START TRANSACTION; ";
                    cmd.CommandText += "INSERT INTO transaction(sid, uid, payment_method, date, confirmed)" +
                        "VALUES(@sid, @uid, @payment_method, @date, @confirmed);";
                    cmd.CommandText += "COMMIT;";
                    cmd.Prepare();
                    //->add parameters to query
                    cmd.Parameters.AddWithValue("@sid", sid);
                    cmd.Parameters.AddWithValue("@uid", uid);
                    cmd.Parameters.AddWithValue("@payment_method", p_method);       //-> Paypal, direct deposit, etc.
                    cmd.Parameters.AddWithValue("@date", DateTime.UtcNow.ToString());       //->Datetime now in current timezone
                    cmd.Parameters.AddWithValue("@confirmed", 1);       //-> we are not actually using a bank, so its always a valid payment
                    await cmd.ExecuteNonQueryAsync();
                    if (conn != null)
                        conn.Close();
                }
                catch (MySqlException ex) 
                {
                    rollback(cmd);
                    if (ex.Number != 1205)
                    {
                        // a sql exception that is not a deadlock 
                        throw;
                    }
                    // Add delay here if you wish. 
                    Thread.Sleep(500);
                    retryCount--;
                    if (retryCount == 0) throw;
                }
            }
        }

       

        public DataTable get_user_transactions_list(int uid)
        {       //->load all apps into datatable for datagridview
            DataTable dt = null;
            int retryCount = 3;
            bool success = false;
            while (retryCount > 0 && !success)
            {
                try
                {
                    conn.Open();
                    //-> select relevant entites for collumns
                    string stm = "SELECT s.title, DATE_FORMAT(t.date, '%d %m %Y') as purchase_date, t.payment_method, s.cost " +
                                 "FROM transaction AS t JOIN view_apps AS s " +
                                 "ON t.uid = " + uid.ToString() + " and t.sid = s.software_id";
                    MySqlDataAdapter da = new MySqlDataAdapter(stm, conn);

                    dt = new DataTable();

                    da.Fill(dt);

                    if (conn != null)
                        conn.Close();
                   // success = true;
                    return dt;
                }
                catch (MySqlException ex)
                {
                    if (ex.Number != 1205)
                    {
                        // a sql exception that is not a deadlock 
                        throw;
                    }
                    // Add delay here if you wish. 
                    Thread.Sleep(500);
                    retryCount--;
                    if (retryCount == 0) throw;
                }
            }
            return dt;
        }

        private string md5_encrypt(string password)
        {       //-> turn all plaintext passwords into one way MD5 ciphers for comparison
        string md5;

        byte[] encodedPassword = new UTF8Encoding().GetBytes(password);

        // need MD5 to calculate the hash
        byte[] hash = ((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(encodedPassword);

        // string representation (similar to UNIX format)
        md5 = BitConverter.ToString(hash)
            // without dashes
           .Replace("-", string.Empty)
            // make lowercase
           .ToLower();

        // encoded contains the hash you are wanting
        return md5;
        }

        public async void rollback(MySqlCommand cmdt)
        {       //rolls back incomplete transaction
            MySqlCommand cmd = cmdt;
            cmd.Connection = conn;
            cmd.CommandText = "ROLLBACK; ";
            await cmd.ExecuteNonQueryAsync();
        }
    }
}






