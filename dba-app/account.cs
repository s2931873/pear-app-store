﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;

namespace dba_app
{
    public partial class account : MetroForm
    {
        private db dba = new db();
        private int user_id;
        public account(int u_id)
        {
            InitializeComponent();
            user_id = u_id;
        }

        private void account_Load(object sender, EventArgs e)
        {       //-> onload: fill out textboxes with user values and fill database                   
            fill();
        }

        private void fill()
        {       //-> prefil form with most user account details
                // and load users transactions 
            ArrayList arr = new ArrayList();
            arr.AddRange(dba.get_user_account_details(user_id));
            metroTextBox2_create_email.Text = arr[0].ToString();
            metroTextBox4_create_name.Text = arr[1].ToString();
            metroTextBox3_create_phone.Text = arr[2].ToString();
            
            metroGrid1.DataSource = dba.get_user_transactions_list(user_id);
        }

        private void update_user_details(object sender, EventArgs e)
        {       //-> get new account details from form elements
            string email = metroTextBox2_create_email.Text.ToString();
            string password = metroTextBox1_create_password.Text.ToString();
            string name = metroTextBox4_create_name.Text.ToString();
            DateTime dob = (DateTime)metroDateTime1.Value;
            string phone = metroTextBox3_create_phone.Text.ToString();
            if (dba.set_user_account_details(user_id, email, password, name, dob, phone))
            {       //-> update login details from form
                var message = MetroFramework.MetroMessageBox.Show(app_store.ActiveForm, "Account update complete!", "Update login status",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }      
        }
    }
}
