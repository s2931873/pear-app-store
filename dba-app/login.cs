﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;

namespace dba_app
{
    public partial class form1 : MetroForm
    {

        private db dba;       //-> Persistent database handler objecct in form memory

        public form1()
        {
            InitializeComponent();
            try
            {
                dba = new db();
            }
            catch(Exception e)
            {
                MetroFramework.MetroMessageBox.Show(this, "Database Connection Failure: " + e.Message.ToString() , "MySQL Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                throw;
            }
        }

        private void login_attempt_btn(object sender, EventArgs e)
        {           //-> Login button cliecked, attempt to log in user
            string email = metroTextBox1_uname.Text;
            string password = metroTextBox2_pword.Text;
            login_attempt(email, password);
        }

        private void login_attempt(string email, string password)
        {           //-> Log user in if details matchs  
            if (email.Length >= 5 && password.Length >= 5 && dba.login(email, password))
            {       //-> Check if details match if yes continue

                        //-> Get globals from composed class
                int user_id = (int)dba.user_id;
                bool is_dev = dba.is_dev;

                     //->correct password message
               MetroFramework.MetroMessageBox.Show(this, "Login Complete: welcome " + metroTextBox1_uname.Text, "Login Status",
                   MessageBoxButtons.OK, MessageBoxIcon.Information);
                
                      //->open the next form
               app_store frm = new app_store(user_id, is_dev , email);
                frm.Show();
                //keep login persistant
               
                      //->close this form
                this.Visible = false;
            }
            else
            {          //->wrong password error
                MetroFramework.MetroMessageBox.Show(this, "Login Failed: please check your details\n\nEnsure username and passwords are over five characters", "Login Status",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void create_account(object sender, EventArgs e)
        {           //-->input fields to string
            string email = metroTextBox2_create_email.Text;
            string password = metroTextBox1_create_password.Text;
            DateTime dob = (DateTime)metroDateTime1.Value;
            string phone = metroTextBox3_create_phone.Text;
            string name = metroTextBox4_create_name.Text;
            char gender = 'M';
            if(!metroCheckBoxGender.Checked)
                gender = 'F';
            int dev = 0;
            if(metroToggle.Checked)
                dev = 1;

                   
                  
            if (email.Length >= 5 && password.Length >= 5)
            {       //-> validate entries
                //-->insert them in database through handler functions
                dba.create_account(email, password, dob, gender, phone, name, dev);
                MetroFramework.MetroMessageBox.Show(this, "Account creation complete: logging in!", "Login Status",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                //->user account created, try to log them in
                login_attempt(email, password);
            }
            else
            {
                MetroFramework.MetroMessageBox.Show(this, "Create account failed: please ensure username and passwords are over five characters", "Create Account Status",
                   MessageBoxButtons.OK, MessageBoxIcon.Error);
           
            }
        }

        private void password_key_bindings_submit(object sender, KeyPressEventArgs e)
        {       //-> if user presses enter button on password field - submit
            if (e.KeyChar == (char)13)
            {
                login_attempt_btn(null,null);
            }
        }

        public void login_attempt_test(string email, string password)
        {       //-> for use with nUnit
            dba.login(email, password);
        }

        private void change_gender_textbox_text(object sender, EventArgs e)
        {
            if(metroCheckBoxGender.Checked)
                metroCheckBoxGender.Text = "Gender: Male";
            else
                metroCheckBoxGender.Text = "Gender: Female";
        }
    }
}
