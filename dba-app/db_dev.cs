﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;
using System.Threading;

namespace dba_app
{
    class db_dev : db       //-> developer is-a user 
    {
        public db_dev() : base("dev") { }

        public bool login(string uname, string pword)
        {
            if (base.login(uname, pword))
                return true;
            return false;
        }

        public async void add_app_to_store(string title, string description, double cost, int dev_id, bool in_app, string package_loc)
        {
            int retryCount = 3;
            bool success = false;
            MySqlCommand cmd = new MySqlCommand();
            while (retryCount > 0 && !success)
            {
                try
                {
                    conn.Open();

                    //->insert relevant data
                    cmd.Connection = conn;
                    cmd.CommandText = "COMMIT; ";
                    cmd.CommandText += "INSERT INTO software(title, description, dev_id, cost, in_app_purchases, package_location)" +
                        "VALUES(@title, @description, @dev_id, @cost, @in_app_purchases, @package_location);";
                    cmd.CommandText += "COMMIT; ";
                    cmd.Prepare();
                    //->add parameters to query
                    cmd.Parameters.AddWithValue("@title", title);
                    cmd.Parameters.AddWithValue("@description", description);
                    cmd.Parameters.AddWithValue("@dev_id", dev_id);       //->Datetime now in current timezone
                    cmd.Parameters.AddWithValue("@cost", cost);       //-> we are not actually using a bank, so its always a valid payment
                    cmd.Parameters.AddWithValue("@in_app_purchases", in_app);
                    cmd.Parameters.AddWithValue("@package_location", package_loc);

                    await cmd.ExecuteNonQueryAsync();
                    success = true;

                    if (conn != null)
                        conn.Close();
                    return;
                }
                catch (MySqlException ex)
                {
                    rollback(cmd);
                    Thread.Sleep(500);
                    if (ex.Number != 1205)
                    {
                        // a sql exception that is not a deadlock 
                        throw;
                    }
                    // Add delay here if you wish. 
                    retryCount--;
                    if (retryCount == 0) throw;
                }
            }
        }


        public DataTable get_developers_apps_to_grid(int dev_id)
        {
            int retryCount = 3;
            bool success = false;
            while (retryCount > 0 && !success)
            {
                try
                {
                    DataTable dt = null;
                    conn.Open();
                    //-> select relevant entites for collumns
                    string stm = "SELECT software_id, title, cost, description, in_app_purchases," +
                        "package_location FROM software WHERE dev_id = " + dev_id;
                    MySqlDataAdapter da = new MySqlDataAdapter(stm, conn);

                    dt = new DataTable();

                    da.Fill(dt);

                    if (conn != null)
                        conn.Close();
                    success = true;
                    return dt;
                }
                catch (MySqlException ex)
                {
                    if (ex.Number != 1205)
                    {
                        // a sql exception that is not a deadlock 
                        throw;
                    }
                    // Add delay here if you wish. 
                    Thread.Sleep(500);
                    retryCount--;
                    if (retryCount == 0) throw;
                }
            }
            return null;
        }

        public async void delete_software(int uid, int index)
        {
            int retryCount = 3;
            bool success = false;
            MySqlCommand cmd = new MySqlCommand();
            while (retryCount > 0 && !success)
            {
                try
                {
                    // your sql here
                    success = true;
                    conn.Open();

                    //->insert relevant data
                   
                    cmd.Connection = conn;
                    cmd.CommandText = "START TRANSACTION; ";
                    cmd.CommandText += "DELETE FROM software where software_id = @index and dev_id = @dev_id;";
                    cmd.CommandText += "COMMIT; ";
                    cmd.Prepare();
                    //->add parameters to query
                    cmd.Parameters.AddWithValue("@dev_id", uid);
                    cmd.Parameters.AddWithValue("@index", index);
                    cmd.ExecuteNonQueryAsync();
                    if (conn != null)
                        conn.Close();
                }
                catch (MySqlException ex)
                {
                    rollback(cmd);
                    if (ex.Number != 1205)
                    {
                        // a sql exception that is not a deadlock 
                        throw;
                    }
                    // Add delay here if you wish. 
                    Thread.Sleep(500);
                    retryCount--;
                    if (retryCount == 0) throw;
                }
            }
        }
        public async void update_software_tuple(int index, int dev_id, string tuple, string value)
        {
            int retryCount = 3;
            bool success = false;
            MySqlCommand cmd = new MySqlCommand();
            while (retryCount > 0 && !success)
            {
                try
                {
                    // your sql here
                    success = true;
                    conn.Open();
                    //->insert relevant data
                    
                    cmd.Connection = conn;
                    cmd.CommandText = "START TRANSACTION; "; 
                    cmd.CommandText += "Update software SET "+tuple+" = @value "+
                        "WHERE software_id = @index and dev_id = @dev_id;";
                    cmd.CommandText += "COMMIT; ";
                    cmd.Prepare();
                    //->add parameters to query
                    cmd.Parameters.AddWithValue("@dev_id", dev_id);
                    cmd.Parameters.AddWithValue("@index", index);
                    cmd.Parameters.AddWithValue("@value", value);
                    await cmd.ExecuteNonQueryAsync();
                    if (conn != null)
                        conn.Close();
                }
                catch (MySqlException ex)
                {
                    rollback(cmd);
                    if (ex.Number != 1205)
                    {
                        // a sql exception that is not a deadlock 
                        throw;
                    }
                    // Add delay here if you wish. 
                    Thread.Sleep(500);
                    retryCount--;
                    if (retryCount == 0) throw;
                }
            }
        }

        public string get_developer_statistics_downloads(int dev_id)
        {
            MySqlDataReader rdr = null;
            int retryCount = 3;
            bool success = false;
            while (retryCount > 0 && !success)
            {
                try
                {
                    conn.Open();
                    string stm = "SELECT s.title, COUNT(t.trans_id), SUM(s.cost) as profit " +
                        "FROM transaction as t LEFT JOIN software as s ON s.dev_id = "
                        + dev_id + " and t.sid = s.software_id GROUP BY s.title";

                    MySqlCommand cmd = new MySqlCommand(stm, conn);
                    rdr = cmd.ExecuteReader();
                    string downloads="";
                   try
                   {
                        while(rdr.Read())
                        {
                        downloads += "App Name: " + rdr.GetString(0) + "\nDownloads: " + rdr.GetString(1);
                        downloads += "\nTotal Profit: $" + rdr.GetString(2) + "\n\n";
                        }
                   }catch(Exception)
                   {        //-> no data in database for datareader
                   downloads = "No apps purchased yet!";
                   }
                    if (conn != null)
                        conn.Close();
                    if (rdr != null)
                        rdr.Close();

                    success = true;
                    return downloads;
                    }
                catch (MySqlException ex)
                {
                    if (ex.Number != 1205)
                    {
                        // a sql exception that is not a deadlock 
                        throw;
                    }
                    // Add delay here if you wish. 
                    Thread.Sleep(500);
                    retryCount--;
                    if (retryCount == 0) throw;
                }
            }
            return null;
        }
    }
}