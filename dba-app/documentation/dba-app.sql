# Privileges for `dev`@`localhost`
GRANT USAGE ON *.* TO 'dev'@'localhost';
GRANT SELECT, INSERT, UPDATE ON `appstore`.`software` TO 'dev'@'localhost';
GRANT SELECT (dev_id, user_id) ON `appstore`.`users` TO 'dev'@'localhost';
GRANT SELECT ON `appstore`.`transaction` TO 'dev'@'localhost';
# Privileges for `root`@`127.0.0.1`
GRANT ALL PRIVILEGES ON *.* TO 'root'@'127.0.0.1' IDENTIFIED BY PASSWORD '*7B4941A4A727F2722E86D904F24C0301852EF45B' WITH GRANT OPTION;
# Privileges for `root`@`::1`
GRANT ALL PRIVILEGES ON *.* TO 'root'@'::1' IDENTIFIED BY PASSWORD '*7B4941A4A727F2722E86D904F24C0301852EF45B' WITH GRANT OPTION;
# Privileges for `root`@`localhost`
GRANT ALL PRIVILEGES ON *.* TO 'root'@'localhost' IDENTIFIED BY PASSWORD '*7B4941A4A727F2722E86D904F24C0301852EF45B' WITH GRANT OPTION;
GRANT PROXY ON ''@'' TO 'root'@'localhost' WITH GRANT OPTION;
# Privileges for `user`@`localhost`
GRANT USAGE ON *.* TO 'user'@'localhost';
GRANT SELECT ON `appstore`.`get_all_apps` TO 'user'@'localhost';
GRANT SELECT ON `appstore`.`view_apps` TO 'user'@'localhost';
GRANT SELECT ON `appstore`.`view_login` TO 'user'@'localhost';
GRANT SELECT, INSERT, UPDATE ON `appstore`.`transaction` TO 'user'@'localhost';
GRANT SELECT (gender, user_name, phone, dev_id, dob, name, user_id), INSERT (gender, user_name, password, phone, dev_id, dob, name) ON `appstore`.`users` TO 'user'@'localhost';

-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 05, 2015 at 09:55 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `appstore`
--
CREATE DATABASE IF NOT EXISTS `appstore` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `appstore`;

-- --------------------------------------------------------

--
-- Table structure for table `software`
--

DROP TABLE IF EXISTS `software`;
CREATE TABLE IF NOT EXISTS `software` (
  `software_id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(75) NOT NULL,
  `description` varchar(200) NOT NULL,
  `genre_id` int(11) NOT NULL,
  `dev_id` int(11) NOT NULL,
  `cost` int(11) NOT NULL DEFAULT '0',
  `in_app_purchases` tinyint(1) NOT NULL,
  `package_location` varchar(250) NOT NULL,
  PRIMARY KEY (`software_id`),
  UNIQUE KEY `title` (`title`),
  KEY `dev_id` (`dev_id`),
  KEY `cost` (`cost`),
  KEY `genre_id` (`genre_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `software`
--

INSERT INTO `software` (`software_id`, `title`, `description`, `genre_id`, `dev_id`, `cost`, `in_app_purchases`, `package_location`) VALUES
(1, 'Worms3', 'Shooting', 1, 1, 2, 0, ''),
(2, 'cghcg', 'cghcg', 2, 1, 5, 1, 'fthvgj');

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

DROP TABLE IF EXISTS `transaction`;
CREATE TABLE IF NOT EXISTS `transaction` (
  `trans_id` int(11) NOT NULL AUTO_INCREMENT,
  `sid` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `payment_method` varchar(25) NOT NULL,
  `date` timestamp NOT NULL,
  `confirmed` tinyint(1) NOT NULL,
  PRIMARY KEY (`trans_id`),
  KEY `uid` (`uid`),
  KEY `date` (`date`),
  KEY `sid` (`sid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=48 ;

--
-- Dumping data for table `transaction`
--

INSERT INTO `transaction` (`trans_id`, `sid`, `uid`, `payment_method`, `date`, `confirmed`) VALUES
(1, 1, 1, '', '0000-00-00 00:00:00', 0),
(32, 1, 1, 'error', '0000-00-00 00:00:00', 0),
(33, 1, 1, 'paypal', '0000-00-00 00:00:00', 1),
(34, 1, 1, 'error', '0000-00-00 00:00:00', 0),
(35, 1, 1, 'paypal', '0000-00-00 00:00:00', 1),
(36, 1, 1, 'error', '0000-00-00 00:00:00', 0),
(37, 1, 1, 'paypal', '0000-00-00 00:00:00', 1),
(38, 1, 1, 'error', '0000-00-00 00:00:00', 0),
(39, 1, 1, 'paypal', '0000-00-00 00:00:00', 1),
(40, 1, 1, 'error', '0000-00-00 00:00:00', 0),
(41, 1, 1, 'paypal', '0000-00-00 00:00:00', 1),
(42, 1, 1, 'error', '0000-00-00 00:00:00', 0),
(43, 1, 1, 'paypal', '0000-00-00 00:00:00', 1),
(44, 1, 1, 'error', '0000-00-00 00:00:00', 0),
(45, 1, 1, 'paypal', '0000-00-00 00:00:00', 1),
(46, 1, 1, 'error', '0000-00-00 00:00:00', 0),
(47, 1, 1, 'paypal', '0000-00-00 00:00:00', 1);

--
-- Triggers `transaction`
--
DROP TRIGGER IF EXISTS `payment_method_check`;
DELIMITER //
CREATE TRIGGER `payment_method_check` BEFORE INSERT ON `transaction`
 FOR EACH ROW IF
NEW.payment_method = "paypal" OR
NEW.payment_method = "card" OR
NEW.payment_method = "bitcoin"
THEN
SET NEW.confirmed = 1;
ELSE
SET NEW.confirmed = 0;
END IF
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(50) NOT NULL AUTO_INCREMENT,
  `gender` varchar(1) NOT NULL DEFAULT 'M',
  `user_name` varchar(75) NOT NULL,
  `password` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `dob` date NOT NULL,
  `phone` varchar(25) NOT NULL,
  `last_update` date DEFAULT NULL,
  `dev_id` tinyint(1) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_name` (`user_name`,`phone`),
  KEY `name` (`name`),
  KEY `dob` (`dob`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `gender`, `user_name`, `password`, `name`, `dob`, `phone`, `last_update`, `dev_id`) VALUES
(1, 'M', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin', '0000-00-00', 'admin', '2015-06-03', 1),
(2, 'M', 'donkey', '9443b0fceb8c03b6a514a706ea69df0b', 'donkey', '0000-00-00', 'donkey', NULL, 1),
(3, 'M', 'donkey2', '767f43f376d0191aa0c6502f89184548', 'donkey2', '0000-00-00', 'donkey2', NULL, 0),
(4, 'M', 'donkey3', '2be234bdc9a9bc2b878aacac60f8f481', 'donkey3', '0000-00-00', 'donkey3', NULL, 1),
(5, 'M', 'donkey4', '03b35482deb325b530a4b1fe476e5b00', 'donkey4', '0000-00-00', 'donkey4', NULL, 0),
(6, 'M', 'donkey5', '0e5e875a727c209c8a059ef2dd7cfe7c', 'donkey5', '2015-06-05', 'donkey5', NULL, 0);

--
-- Triggers `users`
--
DROP TRIGGER IF EXISTS `add_last_update_time`;
DELIMITER //
CREATE TRIGGER `add_last_update_time` BEFORE UPDATE ON `users`
 FOR EACH ROW SET NEW.last_update = NOW()
//
DELIMITER ;
DROP TRIGGER IF EXISTS `gender_check`;
DELIMITER //
CREATE TRIGGER `gender_check` BEFORE INSERT ON `users`
 FOR EACH ROW IF
NEW.gender != "M" OR
NEW.gender != "F"
THEN
SET NEW.gender = "M";
END IF
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Stand-in structure for view `view_apps`
--
DROP VIEW IF EXISTS `view_apps`;
CREATE TABLE IF NOT EXISTS `view_apps` (
`software_id` int(10)
,`title` varchar(75)
,`cost` int(11)
,`description` varchar(200)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `view_login`
--
DROP VIEW IF EXISTS `view_login`;
CREATE TABLE IF NOT EXISTS `view_login` (
`user_id` int(50)
,`user_name` varchar(75)
,`password` varchar(100)
,`dev_id` tinyint(1)
);
-- --------------------------------------------------------

--
-- Structure for view `view_apps`
--
DROP TABLE IF EXISTS `view_apps`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_apps` AS select `s`.`software_id` AS `software_id`,`s`.`title` AS `title`,`s`.`cost` AS `cost`,`s`.`description` AS `description` from `software` `s`;

-- --------------------------------------------------------

--
-- Structure for view `view_login`
--
DROP TABLE IF EXISTS `view_login`;

CREATE ALGORITHM=MERGE DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_login` AS select `users`.`user_id` AS `user_id`,`users`.`user_name` AS `user_name`,`users`.`password` AS `password`,`users`.`dev_id` AS `dev_id` from `users`;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `software`
--
ALTER TABLE `software`
  ADD CONSTRAINT `software_ibfk_1` FOREIGN KEY (`dev_id`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `transaction`
--
ALTER TABLE `transaction`
  ADD CONSTRAINT `transaction_ibfk_1` FOREIGN KEY (`sid`) REFERENCES `software` (`software_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `transaction_ibfk_2` FOREIGN KEY (`uid`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
